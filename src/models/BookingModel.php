<?php

namespace Src\models;

use Src\controllers\Client;
use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;
	private $helper;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings() {
		return $this->bookingData;
	}

	public function createBooking($data) {
		$bookings = $this->getBookings();
		$client = new Client();
		$data = $this->giveDiscount($client, $data);

		$data['id'] = end($bookings)['id'] + 1;
		$bookings[] = $data;

		$this->helper->putJson($bookings, 'bookings');

		return $data;
	}

	private function giveDiscount($client, $data){
		$avarageDosAge = $client->getAvarageDogsAgeByClient(1);

		if ($avarageDosAge < 10) {
			$discount = $data["price"] * 0.1;
			$data["price"] = $data["price"] - $discount;
		}

		return $data;
	}
}